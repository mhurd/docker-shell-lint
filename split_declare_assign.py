import glob
import re
import sys

def split_declare_assign(file_path):
    with open(file_path, "r+") as file:
        content = file.read()
        content = re.sub(r"(^\s*)local ([a-zA-Z0-9_]+)=(.*)", r"\1local \2\n\1\2=\3", content, flags=re.MULTILINE)
        file.seek(0)
        file.write(content)
        file.truncate()

def main():
    for arg in sys.argv[1:]:
        for filename in glob.glob(arg, recursive=True):
            split_declare_assign(filename)

if __name__ == "__main__":
    main()
