FROM alpine:latest

RUN apk --no-cache add shellcheck
RUN apk --no-cache add bash # Required for shfmt
RUN apk --no-cache add python3

# Install shfmt
RUN wget -O shfmt https://github.com/mvdan/sh/releases/download/v3.6.0/shfmt_v3.6.0_linux_amd64 \
    && chmod +x shfmt \
    && mv shfmt /usr/local/bin/

COPY split_declare_assign.py /usr/local/bin/

WORKDIR /scripts

CMD ["bash"]
