# Docker Shell Lint

This project includes a Docker setup to lint, check issues, and format shell script files.

## Building the Docker Image

Build the Docker image by running:
```bash
docker build -t docker-shell-lint .
````

## Linting Shell Scripts

### Current Directory

Lint shell script files in the current directory with:
```bash
docker run --rm -v "$PWD:/scripts" docker-shell-lint shfmt -i 2 -w *.sh
````

### Recursively in All Subdirectories

Lint shell script files recursively in all subdirectories with:
```bash
docker run --rm -v "$PWD:/scripts" docker-shell-lint /bin/sh -c "find /scripts -name '*.sh' -print0 | xargs -0 shfmt -i 2 -w"
````

### Recursive alternative using shfmt image directly (can be used without this repo)
```bash
docker run --rm -u "$(id -u):$(id -g)" -v "$PWD:/mnt/scripts" -w /mnt/scripts mvdan/shfmt:latest-alpine /bin/sh -c "find /mnt/scripts -name '*.sh' -print0 | xargs -0 shfmt -i 2 -w"
````

Note: `-i 2` sets the indentation to 2 spaces and `-w` updates the file in place.

## Checking Shell Script Issues

### Current Directory

Check issues in shell script files in the current directory with:
```bash
docker run --rm -v "$PWD:/scripts" docker-shell-lint shellcheck *.sh
````

### Recursively in All Subdirectories

Check issues in shell script files recursively in all subdirectories with:
```bash
docker run --rm -v "$PWD:/scripts" docker-shell-lint /bin/sh -c "find /scripts -name '*.sh' -print0 | xargs -0 shellcheck"
````

### Recursive alternative using shellcheck image directly (can be used without this repo)
```bash
docker run --rm -v "$PWD:/mnt/scripts" -w /mnt/scripts koalaman/shellcheck-alpine sh -c "find /mnt/scripts -name '*.sh' -print0 | xargs -0 shellcheck"
````

## Splitting 'local' Declarations and Assignments into Separate Lines

### Current Directory

Split 'local' declarations and assignments into separate lines in shell script files in the current directory with:
```bash
docker run --rm -v "$PWD:/scripts" docker-shell-lint python3 /usr/local/bin/split_declare_assign.py *.sh
````

### Recursively in All Subdirectories

Split 'local' declarations and assignments into separate lines in shell script files recursively in all subdirectories with:
```bash
docker run --rm -v "$PWD:/scripts" docker-shell-lint /bin/sh -c "find /scripts -name '*.sh' -print0 | xargs -0 python3 /usr/local/bin/split_declare_assign.py"
````
